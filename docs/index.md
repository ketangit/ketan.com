# Welcome to Ketan.com

### Site layout
    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The Site homepage
        ...       # Other markdown pages, images and other files.

```java
def fn():
    pass
```

### Table-1
First Header | Second Header | Third Header
------------ | ------------- | ------------
Content Cell | Content Cell  | Content Cell
Content Cell | Content Cell  | Content Cell

### Table-2
First Header | Second Header | Third Header
------------ | ------------- | ------------
Content Cell | Content Cell  | Content Cell
Content Cell | Content Cell  | Content Cell

```bash
 cat /etc/password
```

!!! note "Title abced"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.